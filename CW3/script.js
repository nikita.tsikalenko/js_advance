/**
 * Написати функціонал для тултипів
 *
 * Налаштування:
 * - текст
 * - позиціонування (зверху, справа, знизу, зліва), яке за замовчуванням зверху
 * - можливості відображення (при наведенні чи при кліку), за замовчуванням при наведенні
 *
 */

class Tooltip {
    constructor(targetSelector, text, placement, trigger = 'hover') {
        this.targetSelector = targetSelector;
        this.text = text;
        this.placement = placement;
        this.trigger = trigger;
        this.targetNode = document.querySelector(targetSelector);
        this.isShow = false;
        this.isEnebled = true;
        this.render();
        this.attachListener();
    }

    render() {
        this.targetNode.classList.add('tooltip');
        this.targetNode.classList.add(`tooltip-${this.placement}`);
        this.container = document.createElement('div');
        this.container.textContent = this.text;
        this.container.classList.add('tooltip-content')
        this.targetNode.append(this.container);
    }

    attachListener() {
        if (this.trigger === 'hover') {
            this.targetNode.addEventListener('mouseenter', () => {
                this.show();
            })
            this.targetNode.addEventListener('mouseleave', () => {
                this.hide();
            })
        } else if (this.trigger === 'focus') {
            this.targetNode.querySelector('input').addEventListener('focus', () => {
                this.show();
            })
            this.targetNode.querySelector('input').addEventListener('blur', () => {
                this.hide();
            })
        } else {
            this.targetNode.addEventListener(this.trigger, () => {
                this.changeCondition();
            })
        }
    }

    disable() {
        this.isEnebled = false;
    }

    enable() {
        this.isEnebled = true;
    }

    changeCondition() {
        if (this.isShow) {
            this.hide();
            this.isShow = false;
        } else {
            this.show();
            this.isShow = true;
        }
    }

    show() {
        if (this.isEnebled) {
            this.container.classList.add('block');
            this.isShow = true;
        }
    }

    hide() {
        this.container.classList.remove('block');
        this.isShow = false;
    }

    changeText(newText) {
        this.text = newText;
        this.container.textContent = newText;
    }

    changePlacement(newPlacement) {
        this.targetNode.classList.remove(`tooltip-${this.placement}`);
        this.placement = newPlacement;
        this.targetNode.classList.add(`tooltip-${this.placement}`);
    }
}

const firstTooltip = new Tooltip('#btn-1', 'It\'s first button', 'up');
const secondTooltip = new Tooltip('#btn-2', 'It\'s second button', 'right');
const thirdTooltip = new Tooltip('#btn-3', 'It\'s third button', 'down');
const fourthTooltip = new Tooltip('#btn-4', 'It\'s fourth button', 'left', 'click');
const fifthTooltip = new Tooltip('#input', 'It\'s input button', 'down', 'focus');

const allToolTip = [firstTooltip, secondTooltip, thirdTooltip, fourthTooltip, fifthTooltip];

document.querySelector('#show-first').addEventListener('click', () => {
    firstTooltip.show();
});
document.querySelector('#hide-first').addEventListener('click', () => {
    firstTooltip.hide();
});
document.querySelector('#disable-first').addEventListener('click', () => {
    thirdTooltip.disable();
});
document.querySelector('#enable-first').addEventListener('click', () => {
    thirdTooltip.enable();
});
document.querySelector('#disable-all').addEventListener('click', () => {
    for (const toolTip of allToolTip) {
        toolTip.disable();
    }
});
document.querySelector('#enable-all').addEventListener('click', () => {
    for (const toolTip of allToolTip) {
        toolTip.enable();
    }
});
document.querySelector('#apply-align-1').addEventListener('click', function () {
    secondTooltip.changeText(this.previousElementSibling.value);
})
document.querySelector('#apply-align-2').addEventListener('click', function () {
    fourthTooltip.changePlacement(this.previousElementSibling.value);
})

