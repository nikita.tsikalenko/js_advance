class Game {
    constructor(parentNode) {
        this._parentNode = parentNode;
        this.attachListener();
        this._game = new Table('game', '', 10, 10, this._parentNode)
    }

    attachListener() {
        const difficult = document.querySelectorAll('.difficult-variant');
        const startBtn = document.querySelector('#start-game');
        startBtn.addEventListener('click', () => {
            startBtn.textContent = 'Reset';
            this.removeGame();
            let chosenDifficult;
            for (const dif of difficult) {
                if (dif.checked) {
                    chosenDifficult = dif.value;
                }
            }
            const difficulty = [{id: 'easy', time: 1500}, {id: 'medium', time: 1000}, {id: 'hard', time: 500}]
            this.startGame(difficulty.find((difficultyElem)=>difficultyElem.id === chosenDifficult).time);
        })
    }

    removeGame() {
        if (this._game) {
            clearInterval(this._interval)
            if (this._winner) {
                this._winner.remove();
            }
            document.querySelector('#score-pc').textContent = '0';
            document.querySelector('#score-user').textContent = '0';
            this._game.node.remove();
            this._gameEnded = false;
        }
    }

    checkWinner(score, text) {
        if (score > this._game.size / 2) {
            this._winner = document.createElement('p');
            this._winner.textContent = text;
            this._game.node.after(this._winner);
            clearInterval(this._interval);
            this._gameEnded = true;
        }
    }

    setCellWinner(cell, newOwner) {
        if (cell.status === 'active') {
            cell.changeStatus(newOwner);
            const node = document.querySelector(`#score-${newOwner}`)
            node.textContent = String(parseInt(node.textContent) + 1);
            this.checkWinner(parseInt(node.textContent), `${newOwner.toUpperCase()} win!`);
        }
    }

    startGame(time) {
        this._game.render();
        this._game.node.addEventListener('click', (event) => {
            if (event.target.classList.contains('cell') && event.target.classList.contains('active')) {
                const cell = this._game.cellList[event.target.textContent];
                this.setCellWinner(cell, 'user');
            }
        })

        let previousCell;
        this._interval = setInterval(() => {
            if (previousCell) {
                this.setCellWinner(previousCell, 'pc');
            }
            if (!this._gameEnded) {
                let actualCell;
                while (true) {
                    const randomCell = this._game.cellList[parseInt(String(Math.random() * this._game.size))]
                    if (randomCell.isNeutral()) {
                        actualCell = randomCell;
                        break;
                    }
                }
                actualCell.changeStatus('active');
                previousCell = actualCell;
            }
        }, time)
    }
}