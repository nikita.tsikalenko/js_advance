class Cell {
    constructor(clasList, text, parentNode) {
        this._classes = clasList;
        this._status = 'neutral';
        this._text = text;
        this._parentNode = parentNode;
        this.render();
    }

    render() {
        this._node = document.createElement('td');
        this._node.className = this._classes;
        this._node.classList.add(this.status);
        this._node.textContent = this._text;
        this._parentNode.append(this._node);
    }

    changeStatus(newStatus) {
        this._node.classList.remove(this._status);
        this._status = newStatus;
        this._node.classList.add(this._status);
    }

    isNeutral() {
        return this.status === 'neutral';
    }

    get status() {
        return this._status;
    }
}