class Table {
    constructor(id, classList, rows, columns, parentNode) {
        this._id = id;
        this._classList = classList;
        this._rows = rows;
        this._columns = columns;
        this._parentNode = parentNode;
        this._size = this._rows * this._columns;
        this.render();
    }

    render() {
        this._node = document.createElement('table');
        this._node.className = this._classList;
        this._node.id = this._id;
        this._cellList = [];
        for (let i = 0; i < this._rows; i++) {
            const tr = document.createElement('tr');
            for (let j = 0; j < this._columns; j++) {
                const cell = new Cell('cell', i * this._columns + j, tr)
                this._cellList.push(cell);
            }
            this.node.append(tr);
        }
        this._parentNode.append(this._node);
    }

    get node() {
        return this._node;
    }

    get cellList() {
        return this._cellList;
    }

    get size() {
        return this._size;
    }
}