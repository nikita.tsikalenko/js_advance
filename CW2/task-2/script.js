/**
 * Написати клас Modal, за допомогою якого створити 2 об'єкта
 * модальних вікон: loginModal та signUpModal
 *
 * * loginModal має наступні параметри:
 * id - 'login-modal'
 * text - 'Ви успішно увійшли'
 * classList - 'modal login-modal'
 *
 * signUpModal має наступні параметри:
 * id - 'sign-up-modal'
 * text - 'Реєстрація'
 * classList - 'modal sign-up-modal'
 *
 * Кожне модальне вікно обов'язково має наступні методи:
 * - render() - генерує html код модального вікна
 * - open() - показує модальне вікно
 * - close() - закриває модальне вікно
 *
 * - За допомогою методу redner() додати html код
 * модальних вікок в кінець body
 * - При натисканні на кнопку Login за допомогою методу openModal
 * відкривати модальне вікно loginModal
 * - При натисканні на кнопку Sign Up за допомогою методу openModal
 * відкривати модальне вікно signUpModal
 *
 */

class Modal {
    constructor(id, text, classList) {
        this.id = id;
        this.text = text;
        this.classes = classList;
    }

    render(selector) {
        const elem = document.createElement('div');
        elem.id = this.id;
        elem.textContent = this.text;
        elem.className = this.classes;
        console.log(this)
        this.node = elem;
        document.querySelector(selector).append(this.node);
    }

    open() {
        console.log(this.node)
        if (this.node) {
            this.node.classList.add('active');
        }
    }

    close() {
        if (this.node) {
            this.node.classList.remove('active');
        }
    }
}

const loginModal = new Modal('login-modal', 'Ви успішно увійшли', 'modal login-modal');
const signUpModal = new Modal('sign-up-modal', 'Реестрація', 'modal sign-up-modal');

loginModal.render('body');
signUpModal.render('body');

document.querySelector('#login-btn').addEventListener('click', () => {
    loginModal.open()
});
document.querySelector('#sign-up-btn').addEventListener('click', () => {
    signUpModal.open()
});