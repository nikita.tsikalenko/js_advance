class Device {
    constructor(manufacturer, model, price) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.price = price;
        this.margin = 1.2
    }

    getName() {
        return `${this.manufacturer} ${this.model}`
    }

    getPrice() {
        return this.price *= this.margin;
    }
}

class Phone extends Device {
    constructor(manufacturer, model, price, nfc, premium) {
        super(manufacturer, model, price);
        this.nfc = nfc;
        this.margin = premium ? 1.35 : 1.2;
    }

    getShippingPrice() {
        const finalPrice = this.getPrice();
        return finalPrice > 10000 ? finalPrice * 0.01 : finalPrice * 0.03;
    }
}

class Tablet extends Device {
    constructor(manufacturer, model, price, sim) {
        super(manufacturer, model, price);
        this.sim = sim;
    }

    getShippingPrice() {
        const finalPrice = this.getPrice();
        return finalPrice > 20000 ? 0 : finalPrice * 0.01;
    }
}

class Laptop extends Device {
    constructor(manufacturer, model, price) {
        super(manufacturer, model, price);
    }

    getShippingPrice() {
        const finalPrice = this.getPrice();
        return finalPrice > 20000 ? 0 : 200;
    }
}

const lenovoL540 = new Laptop('Lenovo', 'L540', 8000);
const lenovoT440 = new Laptop('Lenovo', 'T440', 6500);

const iphone10 = new Phone('Apple', 'iPhone 10', 6500, true, false);
const iphone11 = new Phone('Apple', 'iPhone 11', 10000, true, false);
const iphone12 = new Phone('Apple', 'iPhone 12', 15000, true, true);

const iPad5 = new Tablet('Apple', 'iPad 5', 20000, true);

console.log(lenovoT440.getPrice(), iphone11.getPrice(), iPad5.getPrice());
console.log(lenovoT440.getShippingPrice(), iphone11.getShippingPrice(), iPad5.getShippingPrice());